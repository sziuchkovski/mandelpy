#!/usr/bin/python

import numpy as np
import sys
import time


def mandelbrot(c, max_radius, max_iter):
    iter = 0
    z = 0 + 0j
    while abs(z) <= max_radius and iter < max_iter:
        z = z * z + c
        iter += 1
    return iter


def mandel_ascii(max_iter):
    min_x = -1.8
    min_y = -1.1
    max_x = 0.6
    max_y = 1.1
    y_pixels = 40
    x_pixels = 160
    max_radius = 2.0

    y_coords = np.arange(min_y, max_y, (max_y - min_y) / y_pixels).tolist()
    x_coords = np.arange(min_x, max_x, (max_x - min_x) / x_pixels).tolist()

    # We're printing top to bottom.
    y_coords.reverse()

    for y in y_coords:
        for x in x_coords:
            iter = mandelbrot(x + y * 1j, max_radius, max_iter)

            if iter < max_iter / 5:
                sys.stdout.write('=')
            elif iter < 2 * max_iter / 5:
                sys.stdout.write('-')
            elif iter < 3 * max_iter / 5:
                sys.stdout.write(':')
            elif iter < 4 * max_iter / 5:
                sys.stdout.write('.')
            elif iter < max_iter:
                sys.stdout.write(' ')
            else:
                sys.stdout.write(' ')
        sys.stdout.write('\n')


def main():
    mandel_ascii(64)


if __name__ == '__main__':
    main()
